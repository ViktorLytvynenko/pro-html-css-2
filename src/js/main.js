const burgerButton = document.querySelector(".header-wrapper_button");
const headerNavi = document.querySelector(".header-wrapper_list")
const visibleImage = document.querySelector(".visible");
const hiddenImage = document.querySelector(".hidden");


burgerButton.addEventListener("click", function() {
    headerNavi.classList.toggle("header-wrapper_list-active")
    visibleImage.classList.toggle("visible");
    visibleImage.classList.toggle("hidden");
    hiddenImage.classList.toggle("hidden");
    hiddenImage.classList.toggle("visible");
})

document.addEventListener("click", (event) => {
    const isClickInsideMenu = headerNavi.contains(event.target);
    const isClickInsideBurger = burgerButton.contains(event.target);
    const burgerButtonClosed = document.querySelector(".header-wrapper_button-closed");
    const burgerButtonOpen = document.querySelector(".header-wrapper_button-open");

    if (!isClickInsideMenu && !isClickInsideBurger) {
        headerNavi.classList.remove("header-wrapper_list-active");
        burgerButtonClosed.classList.remove("hidden");
        burgerButtonClosed.classList.add("visible");
        burgerButtonOpen.classList.remove("visible");
        burgerButtonOpen.classList.add("hidden");
    }
});